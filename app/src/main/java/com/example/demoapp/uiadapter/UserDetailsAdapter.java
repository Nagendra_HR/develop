package com.example.demoapp.uiadapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demoapp.R;
import com.example.demoapp.UserInfo;

import java.util.ArrayList;

public class UserDetailsAdapter extends RecyclerView.Adapter<UserDetailsAdapter.UserViewHolder> {
    private ArrayList<UserInfo> userList;
    private Context mContext;

    public UserDetailsAdapter(ArrayList<UserInfo> userList, Context mContext) {
        this.userList = userList;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View userview = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_row_data,parent,false);

        return new UserViewHolder(userview);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        UserInfo info = userList.get(position);
        holder.userName.setText(info.getUserName());
        holder.userDes.setText(info.getDesignation());

    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class UserViewHolder extends RecyclerView.ViewHolder {
        TextView userName,userDes;
        public UserViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.username);
            userDes = itemView.findViewById(R.id.userdesignation);
        }
    }
}
