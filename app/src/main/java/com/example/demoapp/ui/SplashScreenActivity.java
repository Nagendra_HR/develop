package com.example.demoapp.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demoapp.R;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        try {
            FileOutputStream login =openFileOutput("userlogin.txt",MODE_PRIVATE);
            OutputStreamWriter logWriter = new OutputStreamWriter(login);
            logWriter.write("username:Keerthi,password:12345");
            logWriter.flush();
            logWriter.close();
            FileOutputStream userfile=openFileOutput("userSampleDetails.txt",MODE_PRIVATE);
            OutputStreamWriter userWriter = new OutputStreamWriter(userfile);
            userWriter.write("Name: Keerthi,Designation: Associate Software\nName: Harish,Designation: Associate Software\nName: Sreenu,Designation: Associate Software\nName: Sunil,Designation: Associate Software\nName: Raju,Designation: Assistant Software\n");
            userWriter.flush();
            userWriter.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread timer= new Thread() {
            public void run() {
                try
                {
                    //Display for 3 seconds
                    sleep(3000);
                }
                catch (InterruptedException e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                finally {
                    //Goes to Activity  StartingPoint
                    Intent intent=new Intent(SplashScreenActivity.this,LogInActivity.class);
                    startActivity(intent);
                }
            }
        };
        timer.start();
    }
    //Destroy SplashScreenActivity.java after it goes to next activity
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
