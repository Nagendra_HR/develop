package com.example.demoapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import com.example.demoapp.R;
import com.example.demoapp.UserInfo;
import com.example.demoapp.uiadapter.UserDetailsAdapter;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView viewuser = findViewById(R.id.userList);

        try {
            FileInputStream userStrem = openFileInput("userSampleDetails.txt");
            InputStreamReader userRead = new InputStreamReader(userStrem);
            char  [] inputBuffer = new char[1000];
            int userChar;
            String resultStr="";
            while ((userChar = userRead.read(inputBuffer))>0){
                String val = String.copyValueOf(inputBuffer,0,userChar);
                resultStr+=val;
            }

            userRead.close();
            String [] splituser = resultStr.split("\n");
            ArrayList<UserInfo> userList = new ArrayList<UserInfo>();
            for (String userData: splituser){
                UserInfo newinfo = new UserInfo();
               String[] userdetails  =  userData.split(",");


                   newinfo.setUserName(userdetails[0]);
                   newinfo.setDesignation(userdetails[1]);
                   userList.add(newinfo);


            }

            UserDetailsAdapter userDetailsAdapter = new UserDetailsAdapter(userList,this);
            viewuser.setAdapter(userDetailsAdapter);

        }catch (Exception err){

        }


    }
}
