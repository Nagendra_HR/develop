package com.example.demoapp.ui;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.demoapp.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogInActivity extends AppCompatActivity {
    EditText username,userPassword;
    Button hideShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        username=findViewById(R.id.username);
        userPassword=findViewById(R.id.userPassword);
        hideShow=findViewById(R.id.showHideWord);


        hideShow.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                if(userPassword.getTransformationMethod ().equals ( HideReturnsTransformationMethod.getInstance () )){
                    userPassword.setTransformationMethod ( PasswordTransformationMethod.getInstance () );
                    userPassword.setSelection ( userPassword.getText ().length () );
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        v.setBackground ( getResources ().getDrawable ( R.drawable.ic_action_show ) );
                    }else{
                        v.setBackgroundResource ( R.drawable.ic_action_show );
                    }
                }else{
                    userPassword.setTransformationMethod ( HideReturnsTransformationMethod.getInstance () );
                    userPassword.setSelection ( userPassword.getText ().length () );
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        v.setBackground ( getResources ().getDrawable ( R.drawable.ic_action_hide ) );
                    }else{
                        v.setBackgroundResource ( R.drawable.ic_action_hide );
                    }
                }
            }
        } );

        Button login = findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
                FileInputStream logStrem = null;
                try {
                    logStrem = openFileInput("userlogin.txt");
                    InputStreamReader logRead = new InputStreamReader(logStrem);
                    char  [] inputBuffer = new char[1000];
                    int userChar;
                    String resultStr="";
                    while ((userChar = logRead.read(inputBuffer))>0){
                        String val = String.copyValueOf(inputBuffer,0,userChar);
                        resultStr+=val;
                    }



                    logRead.close();
                 String user =   username.getText().toString();
                 String userpass =   userPassword.getText().toString();
                    Pattern pattern = Pattern.compile(user);
                    Pattern patternPass = Pattern.compile(userpass);
                    Matcher usermatch = pattern.matcher(resultStr);
                    Matcher passmatch = patternPass.matcher(resultStr);
                    if(TextUtils.isEmpty(user)){
                        username.setError("Please enter correct user");
                        return;
                    } else if(usermatch.find() ){
                        int start = usermatch.start();
                        int end = usermatch.end();
                        String validuser = resultStr.substring(start,end);
                        if (!validuser.equals("Keerthi")){
                            username.setError("Please enter correct user");
                            return;
                        }else if (TextUtils.isEmpty(userpass)){
                            userPassword.setError("Please enter correct password");
                            return;
                        }else if(passmatch.find()){
                            int pstart = passmatch.start();
                            int pend = passmatch.end();
                            String validpass= resultStr.substring(pstart,pend);
                            if(!validpass.equals("12345")){
                                userPassword.setError("Please enter correct password");
                                return;
                            }

                        }else {
                            userPassword.setError("Please enter correct password");
                            return;
                        }

                    }else{
                        username.setError("Please enter correct user");
                        return;
                    }



                        Intent dash = new Intent(LogInActivity.this,MainActivity.class);
                        startActivity(dash);


                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });


    }
}
